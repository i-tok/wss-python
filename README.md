# wss-pythpn

#### 介绍
一个好用的命令行版文叔叔上传下载工具

#### 安装教程

```
pip3 install wss-python  -i https://pypi.python.org/simple/
```

#### 使用说明

1. upload:
```
wss upload <file>
```
2. download:
```
wss download <url>
```
